<%@page import="javax.portlet.ActionRequest"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@page import="com.liferay.strongauth.web.internal.configuration.StrongAuthConstants"%>
<%@page import="com.liferay.strongauth.web.internal.configuration.StrongAuthConfiguration"%>
<%@page import="com.liferay.portal.kernel.settings.CompanyServiceSettingsLocator"%>
<%@page import="com.liferay.portal.kernel.settings.ParameterMapSettingsLocator"%>
<%@page import="com.liferay.portal.kernel.module.configuration.ConfigurationProviderUtil"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
StrongAuthConfiguration strongAuthConfiguration = ConfigurationProviderUtil.getConfiguration(StrongAuthConfiguration.class,
		new ParameterMapSettingsLocator(request.getParameterMap(), "strongauth--", new CompanyServiceSettingsLocator(company.getCompanyId(), StrongAuthConstants.SERVICE_NAME)));
String controlPanelCode = strongAuthConfiguration.controlPanelCode();
%>

<aui:input name="<%= ActionRequest.ACTION_NAME %>" type="hidden" value="/portal_settings/strongauth" />
<aui:input type="text" wrapperCssClass="lfr-input-text-container" label="code" name="strongauth--controlPanelCode" value="<%= controlPanelCode %>" />
