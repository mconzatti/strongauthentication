package com.liferay.strongauth.web.internal.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "foundation")
@Meta.OCD(
	id = "com.liferay.strongauth.web.internal.configuration.StrongAuthConfiguration",
	localization = "content/Language",
	name = "strongauth.configuration.name"
)
public interface StrongAuthConfiguration {
	
	@Meta.AD(
		deflt = "",
		description = "Control Panel Code",
		required = false
	)
	public String controlPanelCode();
	
}
