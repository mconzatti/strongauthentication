package com.liferay.strongauth.web.internal.portlet.action;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.settings.portlet.action.BasePortalSettingsFormMVCActionCommand;
import com.liferay.portal.settings.web.constants.PortalSettingsPortletKeys;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;

@Component(
	immediate = true,
	property = {
		"javax.portlet.name=" + PortalSettingsPortletKeys.PORTAL_SETTINGS,
		"mvc.command.name=/portal_settings/strongauth"
	},
	service = MVCActionCommand.class
)
public class StrongAuthPortalSettingsAuthenticationFormMVCActionCommand extends BasePortalSettingsFormMVCActionCommand {

	private static Log _log = LogFactoryUtil.getLog(StrongAuthPortalSettingsAuthenticationFormMVCActionCommand.class);

	@Override
	protected void doValidateForm(ActionRequest actionRequest, ActionResponse actionResponse) {
		_log.info("StrongAuthPortalSettingsAuthenticationFormMVCActionCommand.doValidate");
	}
	
	@Override
	protected String getParameterNamespace() {
		return "strongauth--";
	}
	
	@Override
	protected String getSettingsId() {
		return "com.liferay.portal.security.sso.strongauth";
	}
	
	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		System.out.println("StrongAuthPortalSettingsAuthenticationFormMVCActionCommand.doProcessAction");
		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		storeSettings(actionRequest, themeDisplay);
	}
	
}
