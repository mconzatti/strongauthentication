package com.liferay.strongauth.web.internal.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;

import org.osgi.service.component.annotations.Component;

@Component
public class StrongAuthCompanyServiceConfigurationPidMapping implements ConfigurationPidMapping {

	@Override
	public Class<?> getConfigurationBeanClass() {
		return StrongAuthConfiguration.class;
	}

	@Override
	public String getConfigurationPid() {
		return StrongAuthConstants.SERVICE_NAME;
	}

}