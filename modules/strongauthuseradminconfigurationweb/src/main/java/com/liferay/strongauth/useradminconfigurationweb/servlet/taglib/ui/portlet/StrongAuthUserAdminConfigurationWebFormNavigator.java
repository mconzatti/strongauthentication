package com.liferay.strongauth.useradminconfigurationweb.servlet.taglib.ui.portlet;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.taglib.ui.BaseJSPFormNavigatorEntry;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorConstants;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorEntry;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
	immediate = true,
	property = {"form.navigator.entry.order:Integer=75"},
	service = FormNavigatorEntry.class
)

public class StrongAuthUserAdminConfigurationWebFormNavigator extends BaseJSPFormNavigatorEntry<User> {

	@Override
	protected String getJspPath() {
		return "/user/strongauth.jsp";
	}

	@Override
	public String getCategoryKey() {
		return FormNavigatorConstants.CATEGORY_KEY_USER_IDENTIFICATION;
	}

	@Override
	public String getKey() {
		return "StrongAuthentication";
	}

	@Override
	public boolean isVisible(User user, User selUser) {
		if (selUser == null) {
			return false;
		}
		return true;
	}

	@Override
	public String getFormNavigatorId() {
		return FormNavigatorConstants.FORM_NAVIGATOR_ID_USERS;
	}

	@Override
	public String getLabel(Locale locale) {
		return LanguageUtil.get(locale, getKey());
	}

	@Override
	public void include(HttpServletRequest request, HttpServletResponse response) throws IOException {
		super.include(request, response);
	}

	@Override
	@Reference(
		target = "(osgi.web.symbolicname=strongauthuseradminconfigurationweb)",
		unbind = "-"
	)
	public void setServletContext(ServletContext servletContext) {
		super.setServletContext(servletContext);
	}
}