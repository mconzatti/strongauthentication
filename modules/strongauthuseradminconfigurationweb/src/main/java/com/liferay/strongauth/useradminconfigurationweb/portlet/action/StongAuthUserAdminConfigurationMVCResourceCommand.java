package com.liferay.strongauth.useradminconfigurationweb.portlet.action;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.servlet.ServletResponseUtil;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;

@Component(
	immediate = true,
	property = {
		"javax.portlet.name=com_liferay_my_account_web_portlet_MyAccountPortlet",
		"javax.portlet.resource-bundle=content.Language",
		"mvc.command.name=/users_admin/strongauthentication"
	},
	service = MVCResourceCommand.class
)

public class StongAuthUserAdminConfigurationMVCResourceCommand extends BaseMVCResourceCommand {

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws Exception {
		String code = ParamUtil.getString(resourceRequest, "code");
		System.out.println("code = " + code);
		
		HttpServletResponse httpResponse = PortalUtil.getHttpServletResponse(resourceResponse);
		httpResponse.setContentType(ContentTypes.APPLICATION_JSON);
		JSONObject jsonResponse = JSONFactoryUtil.createJSONObject();
		jsonResponse.put("status", "OK");
		jsonResponse.put("message", code);
		ServletResponseUtil.write(httpResponse, jsonResponse.toString());
	}

}
