<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>


<liferay-portlet:resourceURL id="/users_admin/strongauthentication" var="saveCodeURL" />

<aui:input label="code" id="code" name="code" type="text" />

<aui:button id="saveCodeButton" type="button" value="save" onclick="saveCode()"/>

<aui:script>
	Liferay.provide(
		window,
		'saveCode',
		function() {
			var A = AUI();
			A.one('#<portlet:namespace />saveCodeButton').attr({'disabled' : true});
			var url = '${saveCodeURL}';
			var code = A.one('#<portlet:namespace />code').get('value');
			url = url + '&<portlet:namespace />code=' + code;

			A.io.request(
				url,
				{
					dataType: 'json',
					on: {
						success: function(event, id, obj) {
							var instance = this;
							var response = instance.get('responseData');
							if (response) {
								if (response.status) {
									alert('<liferay-ui:message key="success"/>'+': '+response.message);
								} else if (response.message) {
									alert('<liferay-ui:message key="fail"/>');
								} else {
									alert('<liferay-ui:message key="fail"/>');
								}
							} else {
								alert('<liferay-ui:message key="fail"/>'+': Invalid response');
							}
							A.one('#<portlet:namespace />code').set('value', '');
							A.one('#<portlet:namespace />saveCodeButton').attr({'disabled' : false});
						}
					}
				}
			);
		},
		['aui-io']
	);
</aui:script>