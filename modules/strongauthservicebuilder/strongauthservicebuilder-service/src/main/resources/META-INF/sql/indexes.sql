create index IX_DB5F8852 on strongauthentication_Config (userId);
create index IX_6691740C on strongauthentication_Config (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_11BF8D8E on strongauthentication_Config (uuid_[$COLUMN_LENGTH:75$], groupId);