create table strongauthentication_Config (
	uuid_ VARCHAR(75) null,
	configId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	status BOOLEAN
);